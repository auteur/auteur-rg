#!/bin/sh
# Auteur NLE 
# by klaatu
# contact klaatu at member.fsf.org
#

########################################
# this is the clipmaker module
# where we review the raw footage
# and decide on the takes we like best
#
# you can see that basically we just have
# mplayer play the clip, directing the edit decisions
# to a tmp file which is technically disposable after
# the next module (the clipdefiner) is complete
######################################################

echo "Here you will create a new clip."
echo "Enter the path to the source footage your clip is in."
read source
echo "$source will now begin playback. To mark your In point, hit i"
echo "To mark your Out point, hit i again."

#####################################################
# just because ive always been told to give everything
# a unique identifier, we create a unique identifier with
# date plus cutting out the timestamp plus replacing the 
# colons with dashes
#########################################################

clipid=$(date | cut -f 4 -d " " | sed s_:_-_g)

#### next we actually start playing the clip ####


mplayer -edlout ./tmp/$source.$clipid.cdl $source


#####################################################
# next we name the clip
# but more importantly we translate the mplayer edl to something
# we can play back
################################################################## 

echo "You have defined a clip.  Please type a name for your clip."
read clipname

###############################################
# taking the in point gives us start position
# and out point minus in point equals 
#           how long to play the clip
#############################################

ipoint=$(cat ./tmp/$source.$clipid.cdl | cut -f 1 -d " ")
opoint=$(cat ./tmp/$source.$clipid.cdl | cut -f 2 -d " ")
dur=$(echo $opoint-$ipoint | bc)

############################################
# input all the data we have just calculated
# into a new permanent-ish file that now becomes 
# the representation of that clip
# which we can playback and it will look like a 
# real clip
###################################################

echo "-ss $ipoint -endpos $dur $source" > ./clips/$clipname
# chmod +x ./clips/$clipname

###############################################
# and say something nice to let the user know 
# where their clip is kept
################################################

echo "now you have a clip called $clipname in the clips directory of your project"
