#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtCore, QtGui 

try:
    raw_input
    PYTHON3 = False
except NameError:
    PYTHON3 = True
    
class DontAskAgainDialog(QtGui.QDialog):
    def __init__(self, parent=None, question = None):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(_("Confirm"))
        
        if question is None:
            question = _("Are You Sure?")
        
        icon_label = QtGui.QLabel()
        icon_label.setPixmap(QtGui.QPixmap(":images/q-auteur.png"))
        
        label = QtGui.QLabel()
        label.setWordWrap(True)
        label.setText(question)
        
        self.check_box = QtGui.QCheckBox(_("Don't ask again"))
        
        button_box = QtGui.QDialogButtonBox(
            QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)        
        
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)
        
        layout = QtGui.QGridLayout(self)
        layout.setSpacing(3)
        layout.addWidget(icon_label,0,0)
        layout.addWidget(label,0,1)
        layout.addWidget(self.check_box,1,0)
        layout.addWidget(button_box,1,1)
        
        self.setMinimumSize(300,150)

    def sizeHint(self):
        return QtCore.QSize(350,200)
    
    def exec_(self):
        result = QtGui.QMessageBox.exec_(self)
        return (result, self.check_box.isChecked())
        

class QAuteurSettings(QtCore.QSettings):
    def __init__(self, parent=None):
        QtCore.QSettings.__init__(self, parent)

    @property
    def PYTHON3(self):
        '''
        returns True if the interpreter is Python3, False if Python2
        '''
        return PYTHON3
        
    @property
    def video_path(self):
        '''
        the last folder visited for video sources
        '''
        val = self.value("video_path")
        return val if self.PYTHON3 else val.toString()
            
    @property
    def workspace_path(self):
        '''
        the last folder visited when loading/saving workspaces
        '''
        val = self.value("workspace_path")
        return val if self.PYTHON3 else val.toString()
    
    @property
    def is_docked(self):
        '''
        whether to use MDI or SDI interface
        '''
        val = self.value("is_docked")
                
        if self.PYTHON3:
            if val is None:
                return True
            else:
                return val != "false"
        else:
            if val == QtCore.QVariant():
                return True
            else:
                return val.toBool() 
        
    @property
    def is_multi_window(self):
        '''
        whether to use MDI or SDI interface
        '''
        return not self.is_docked
    
    @property
    def window_state(self):
        if self.is_docked:
            print ("returning docked state")
            return self.docked_window_state        
        else:
            print ("returning multi state")            
            return self.multi_window_state
        
    @property
    def geometry(self):
        if self.is_multi_window:
            return self.multi_geometry
        else:
            return self.docked_geometry        

    @property
    def docked_geometry(self):
        val = self.value("docked_geometry")
        if self.PYTHON3:
            return QtCore.QByteArray() if val is None else val
        else:    
            return val.toByteArray()
    
    @property
    def docked_window_state(self):
        val = self.value("docked_state")
        if val is None:
            raise KeyError
        if self.PYTHON3:
            return QtCore.QByteArray() if val is None else val
        else:    
            return val.toByteArray()
    
    @property
    def multi_geometry(self):
        val = self.value("multi_geometry")
        if val is None:
            raise KeyError
        if self.PYTHON3:
            return QtCore.QByteArray() if val is None else val
        else:    
            return val.toByteArray()
    
    @property
    def multi_window_state(self):
        val = self.value("multi_state")
        if self.PYTHON3:
            return QtCore.QByteArray() if val is None else val
        else:    
            return val.toByteArray()
    
    
    @property
    def native_mplayer(self):
        val = self.value("native_mplayer")
        return False#True
        return bool(val) if self.PYTHON3 else val.toBool()

    @property
    def split_warning_accepted(self):
        option = self.value("ignore_split_warning")
        result = bool(option) if self.PYTHON3 else option.toBool()
        if result:
            return True
            
        question = "%s <br />%s"% (
        _("the source of the video you are watching is already a 'clip'"),
        _("would you like to divide it into two clips at the current position?"))
        
        dl = DontAskAgainDialog(self.parent(), question)
        result, dont_ask = dl.exec_()
        
        self.setValue("ignore_split_warning", dont_ask)
        
        return result
        
def _test(): 
    import gettext, os, sys
    gettext.install("auteur")
    
    sys.path.insert(0, os.path.abspath("../"))
    from lib_q_auteur.components import q_resources
    
    app = QtGui.QApplication([])
    
    settings = QAuteurSettings()
    
    print (settings.split_warning_accepted)

    app.closeAllWindows()
                   
if __name__ == "__main__":
    
    _test()