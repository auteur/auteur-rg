#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import re
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))

from lib_q_auteur.components import ComponentWidget


class WorkflowWidget(ComponentWidget):
    def __init__(self, model, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("WorkFlow Wizard"))
        self.icon = QtGui.QIcon(":images/wizard.png")
        
        self.model = model
        layout = QtGui.QVBoxLayout(self)    
        
        icon = QtGui.QIcon(":images/sources.png")
        self.get_sources_button = QtGui.QPushButton(icon, 
            _("Get Source Videos"))
        
        self.get_sources_button.setObjectName("sources")
        self.get_sources_label = QtGui.QLabel()
        self.get_sources_label.setWordWrap(True)
        self.get_sources_label.setAlignment(QtCore.Qt.AlignCenter)

        icon = QtGui.QIcon(":images/clips.png")
        self.review_sources_button = QtGui.QPushButton(icon, _("review source videos"))
        self.review_sources_button.setObjectName("review")
        self.review_sources_label = QtGui.QLabel(
            "<body>%s, %s<br/>%s</body>"% (
            _("Now you have source video"),
            _("Select the parts you wish to use"),
            _("These parts are reffered to as 'clips'")))
        self.review_sources_label.setWordWrap(True)
        self.review_sources_label.setAlignment(QtCore.Qt.AlignCenter)

        icon = QtGui.QIcon(":images/folder_video.png")
        self.order_clips_button = QtGui.QPushButton(icon, "set clip sequence")
        self.order_clips_button.setObjectName("sequence")
        
        icon = QtGui.QIcon(":images/folder_video.png")
        self.preview_button = QtGui.QPushButton(icon, "preview the edited video")
        self.preview_button.setObjectName("preview")
        
        self.render_button = QtGui.QPushButton(icon, "render the edited video")
        self.render_button.setObjectName("render")
        
        
        self.setStyleSheet('''
            QWidget {background: white;}
            QPushButton#sources {background: #619597;}
            QPushButton#review {background: #599496;}
            QPushButton#sequence {background: #519496;}
            QPushButton#preview {background: #4a9496;}
            QPushButton#render {background: #429496;}
            ''')        
        
        self.pairs = (  
            (   self.get_sources_button, self.get_sources_label         ),
            (   self.review_sources_button, self.review_sources_label   ),
            (   self.order_clips_button, QtGui.QLabel("placeholder")    ),
            (   self.preview_button, QtGui.QLabel("placeholder")        ),
            (   self.render_button, QtGui.QLabel("placeholder")         )
            )
        
        self.showlist = [0]

        for but, label in self.pairs:
            layout.addWidget(label)
            layout.addWidget(but)

        layout.addSpacing(0)

        self.new_project()

    def show(self, showlist):
        self.showlist = showlist
        for i in range(len(self.pairs)):
            but, label = self.pairs[i]
            but.setMinimumHeight(80)
            show = i in self.showlist
            but.setVisible(show)
            label.setVisible(show)
                
    def new_project(self):
        self.get_sources_label.setText("<body>%s<br/><i>%s</i></body>"% (
            _("To get started, please select some video sources."),
            _("Note - these will not be altered in any way by q_auteur")))
        
        self.show([0])
            
    def update_data(self):
        '''
        called whent the model changes
        '''
        n = self.model.number_of_sources 
        if n == 0:
            self.new_project()
            return
        elif n == 1:
            self.get_sources_label.setText(
                "<b>%s</b> %s"%( n, _('source loaded')))
        else:
            self.get_sources_label.setText(
                "<b>%s</b> %s"%( n, _('sources loaded')))
        
        self.show([1])
        
    def back(self):
        for i in range(len(self.pairs)):
            if i in self.showlist:
                break   
        
        i = 0 if i==0 else i-1
        self.show([i])
    
    def skip(self):
        max_ = len(self.pairs)
        for i in range(max_):
            if i in self.showlist:
                break   
        
        i =  (max_-1) if (i> max_-1) else i+1
        self.show([i])
    
def _test():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    from lib_q_auteur.components.data_model import DataModel    
    model = DataModel() 
    
    obj = WorkflowWidget(model)
    but1 = QtGui.QPushButton("back")
    but2 = QtGui.QPushButton("skip")
    but1.clicked.connect(obj.back)
    but2.clicked.connect(obj.skip)
    
    
    frame = QtGui.QFrame()
    layout = QtGui.QHBoxLayout(frame)
    layout.addWidget(but1)
    layout.addWidget(but2)
    
    obj.layout().addWidget(frame)
    
    obj.sub_window.show()
    app.exec_()
    
    
if __name__ == "__main__":
    from lib_q_auteur.components import q_resources
    
    _test()
