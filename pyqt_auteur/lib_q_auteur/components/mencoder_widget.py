#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))
    
from lib_q_auteur.components import ComponentWidget

class MencoderWidget(ComponentWidget):
    def __init__(self, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("Mencoder"))
        self.icon = QtGui.QIcon(":images/bash.png")
        
        self.label = QtGui.QLabel("%s %s"% (_("RENDER"), _("COMMAND")))
        self.label.setWordWrap(True)
        self.text_browser = QtGui.QTextEdit()
        self.text_browser.setReadOnly(True)
        self.text_browser.setFont(QtGui.QFont("courier"))
        
        layout = QtGui.QHBoxLayout(self)
        layout.setMargin(0)
        layout.addWidget(self.label)
        layout.addWidget(self.text_browser)
        
        self.outfile = None
        self.options = []
        
    def clear(self):
        self.outfile = None
        self.options = []        
        
    def set_outfile(self, outfile):
        self.outfile = str(outfile).replace(" ", "\ ")
        self.update_data()
        
    def set_options(self, options):
        assert type(options) == type([]), "options must be a list"
        self.options = options
        self.update_data()
        
    def show_label(self, val):
        '''
        set visible for the label
        '''
        self.label.setVisible(val)
        
    def sizeHint(self):
        return QtCore.QSize(200,100)
        
    def set_model(self, model):
        '''
        attach the view to the data_model
        '''
        self.model = model
        self.model.attach_view(self)
    
    def update_data(self):
        data = self.model.mencoder_string
        
        if self.outfile:
            data = data.replace("[YOUR_OUTFILE]", self.outfile)
        opt_str = ""
        for option in self.options:
            opt_str += "%s "% option
        data = data.replace("[OPTIONS]", opt_str)
        
        
        self.text_browser.setText(data)
    
    @property
    def command_list(self):
        text = str(self.text_browser.toPlainText())
        if "\ " in text:            
            text = text.replace("\ ","<*FOO*>")
        
        command_list = text.split(" ")
        
        if "<*FOO*>" in text:
            copied_list = command_list[:]
            command_list = []
            for command in copied_list:
                command_list.append(command.replace("<*FOO*>"," "))
        while "" in command_list:
            command_list.remove("")
        return command_list
            
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = MencoderWidget() 
    
    from lib_q_auteur.components.data_model import DataModel
    
    model = DataModel() 
    model.add_source_file(
        os.path.abspath("test_video/greetings_and_salutations.ogv"))
    
    obj.set_model(model)
    obj.update_data()
    
    obj.show()
    app.exec_()    

if __name__ == "__main__":
    _test_code()
