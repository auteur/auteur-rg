#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from __future__ import division

import os, subprocess
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import sys
    sys.path.insert(0, os.path.abspath("../../"))
    
from lib_q_auteur.components import ComponentWidget

class StillImage(QtGui.QImage):
    def __init__(self, width=640, height=480):
        QtGui.QImage.__init__(self, width, height, QtGui.QImage.Format_RGB32) 
        
        self.fill(255)

        self._text = "Rendered on Linux"

        self._duration = 5
        
    @property
    def duration(self):
        return self._duration
    
    def set_duration(self, dur):
        '''
        set the duration of the video (in seconds)
        '''
        self._duration = dur

    @property
    def text(self):
        return self._text

    def set_text(self, message):
        self._text = message
        
    def set_colour(self, rgb):
        self.fill(rgb)        
        
    def _draw(self):
        painter = QtGui.QPainter(self)
        
        font = QtGui.QApplication.instance().font()
        font.setPointSize(40)
        
        option = QtGui.QTextOption()
        option.setAlignment(QtCore.Qt.AlignCenter)
        
        pen = QtGui.QPen(QtGui.QColor("white"))
        painter.setPen(pen)
        painter.setFont(font)
        painter.drawText(QtCore.QRectF(self.rect()), self.text, option)

    def save(self, location):
        self._draw()
        QtGui.QImage.save(self, location)

    @property
    def mencoder_line(self):
        return '''mencoder -o Desktop/test.avi 
            mf:///home/neil/Desktop/test.png -mf fps=%.2f 
            -ovc lavc -lavcopts vcodec=mpeg4'''% (1/self.duration)


class StillImageWidget(ComponentWidget):
    def __init__(self, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("Still Images"))
        self.icon = QtGui.QIcon(":images/image.png")
    
    def set_model(self, model):
        self.model = model
        
    def update_data(self):
        print (self.model.stills)
        
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    c = StillImage(300,200)
    print ("saving file")
    print (c.save("test_credits.png"))

    obj = StillImageWidget() 
    
    from lib_q_auteur.components.data_model import DataModel
    
    model = DataModel() 
    test_proj = "/home/neil/Desktop/qauteur_prog.xml"
    model.load(test_proj)
    
    obj.set_model(model)
    obj.update_data()
    obj.sub_window.show()

    app.exec_()    

if __name__ == "__main__":
    _test_code()
