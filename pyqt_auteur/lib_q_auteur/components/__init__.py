#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

'''
this is a simple __init__.py script to package the q_auteur compenents
(ie. make import * work)
'''
if __name__ == "__main__":
    print("checking imports working")

    import sys
    sys.path.insert(0, "../../")
    
from lib_q_auteur.components.resources import q_resources
from lib_q_auteur.components.component_base_widget import ComponentWidget

from lib_q_auteur.components.screen_widget import ScreenWidget
from lib_q_auteur.components.video_controls import VideoControls
from lib_q_auteur.components.xml_widget import DataWidget
from lib_q_auteur.components.sources_widget import SourcesWidget
from lib_q_auteur.components.clips_widget import ClipsWidget
from lib_q_auteur.components.data_model import DataModel, Clip, Source
from lib_q_auteur.components.editor_menu import EditorMenu
from lib_q_auteur.components.workflow_widget import WorkflowWidget
from lib_q_auteur.components.mencoder_widget import MencoderWidget
from lib_q_auteur.components.still_image import StillImageWidget
from lib_q_auteur.components.time_line import TimeLineWidget





