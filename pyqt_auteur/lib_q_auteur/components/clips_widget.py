#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, subprocess
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import sys
    sys.path.insert(0, os.path.abspath("../../"))
    
from lib_q_auteur.components.clip_label import ClipLabel
from lib_q_auteur.components import ComponentWidget

class ClipsWidget(ComponentWidget):
    def __init__(self, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("Clips"))
        self.icon = QtGui.QIcon(":images/clips.png")
        self.clip_layout = None

        self.frame = QtGui.QFrame()
        self.frame.setObjectName("dropzone")
        
        label = QtGui.QLabel(_("no clips added yet"))
                
        self.layout = QtGui.QGridLayout(self)
        self.layout.addWidget(label)
        
        self.model = None
        self.setMinimumSize(200,100)
        self.setAcceptDrops(True)
        self.set_bg()

    def sizeHint(self):
        return QtCore.QSize(400,100)
        
    def set_model(self, model):
        '''
        attach the view to the data_model
        '''
        self.model = model
        self.model.attach_view(self)
        
    def add_clip_layout(self):
        self.clip_layout = QtGui.QHBoxLayout(self.frame)
        self.clip_layout.addStretch(0)

        scroll_area = QtGui.QScrollArea(self)
        scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(self.frame)

        icon = QtGui.QIcon.fromTheme("media-playback-start")
        view_button = QtGui.QPushButton(_("Preview"))
        view_button.setIcon(icon)
        
        render_button = QtGui.QPushButton(_("Render"))
        icon = QtGui.QIcon.fromTheme("media-record")
        render_button.setIcon(icon)
        self.length_label = QtGui.QLabel()
        
        self.layout.addWidget(scroll_area,0,0,3,1)
        self.layout.addWidget(self.length_label,0,1)
        self.layout.addWidget(view_button,1,1)
        self.layout.addWidget(render_button,2,1)
        
        view_button.clicked.connect(self.preview)
        render_button.clicked.connect(self.render)
    
    @property
    def clips(self):
        return self.model.clips
    
    def preview(self):
        self.emit(QtCore.SIGNAL("preview"))
        
    def render(self):
        self.emit(QtCore.SIGNAL("render"))
    
    def add_clip(self, clip):
        label = ClipLabel()
        label.clip = clip
        label.setToolTip(clip.details)
        label.setPixmap(clip.thumbnail)
        self.clip_layout.insertWidget(self.clip_layout.count()-1,label)
    
        self.connect(label, QtCore.SIGNAL("clicked"), self.clip_clicked)
        self.connect(label, QtCore.SIGNAL("right clicked"), 
            self.clip_right_clicked)
        
    def deselect_cliplabels(self, label):
        '''
        deselects all but the named label
        '''    
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg != label:
                widg.is_selected = False
                widg.update()
    
    def select_clip_from_id(self, id):
        '''
        selects the clip with given id
        '''    
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg.clip.id == id:
                widg.select()
                break
                
    def clip_clicked(self):
        label = self.sender()
        self.deselect_cliplabels(label)
        if self.selected_clip.exists:
            self.emit(QtCore.SIGNAL("clip selected"), self.selected_clip)
        else:
            QtGui.QMessageBox.warning(self, _("ERROR"),
            _("The File Cannot be found"))
        
    def clip_right_clicked(self, pos):
        ## there was an issue here.. 
        label = self.sender()
        self.deselect_cliplabels(label)
        #self.emit(QtCore.SIGNAL("clip selected"), self.selected_clip)
    
        options = [
            _("Load clip into editor"), 
            _("Advance clip (move left)"),
            _("Retreat clip (move right)"),            
            _("Delete Clip")
            ]
        menu = QtGui.QMenu(self)
        for option in options:
            menu.addAction(option)
        result = menu.exec_(pos)
        if not result:
            return
        if result.text() == options[0]:
            self.clip_clicked()
        elif result.text() == options[1]:
           self.model.advance_clip(label.clip)
        elif result.text() == options[2]:
           self.model.retreat_clip(label.clip)
        elif result.text() == options[3]:
            if QtGui.QMessageBox.question(self, _("confirm"),
            _("delete clip?"),
            QtGui.QMessageBox.Ok|QtGui.QMessageBox.Cancel,
            QtGui.QMessageBox.Ok) == QtGui.QMessageBox.Ok:
                self.emit(QtCore.SIGNAL("delete clip"))
                
    @property
    def selected_clip(self):            
        for i in range(self.clip_layout.count()-1):
            widg = self.clip_layout.itemAt(i).widget()
            if widg.is_selected:
                return widg.clip
            
    def update_data(self):
        if self.clip_layout is None:
            self.add_clip_layout()
        else:
            while self.clip_layout.count()>1:
                item = self.clip_layout.takeAt(0)
                item.widget().deleteLater()
        
        for clip in self.clips:
            self.add_clip(clip)    
            
        self.length_label.setText(
            "Combined Length %d seconds"% self.model.duration)
        
    def dragEnterEvent(self, event):
        if event.mimeData().hasText():
            self.set_bg(True)
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasText():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dragLeaveEvent(self, event):
        self.set_bg()

    def dropEvent(self, event):
        data = event.mimeData()
        if data.hasText():
            data = data.text().split("|")
            dragged = str(data[0]).startswith("DRAGGED")
            if not dragged:
                self.set_bg()
                event.ignore()
                return
            filename = str(data[1])
            try:
                start = float(data[2])
                end = float(data[3])
                self.model.add_clip(filename, start, end)
            except IndexError:
                self.model.use_full_source_as_clip(filename)       
        self.set_bg()
        event.accept()

    def set_bg(self, active=False):
        if active:
            val = "QFrame#dropzone {background:yellow;}"
        else:
            val = "QFrame#dropzone {}"
        self.setStyleSheet(val)

        
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = ClipsWidget() 
    
    from lib_q_auteur.components.data_model import DataModel
    
    model = DataModel() 
    model.add_source_file("test_video/greetings_and_salutations.ogv")
    model.use_full_source_as_clip("test_video/greetings_and_salutations.ogv")
    obj.set_model(model)
    obj.update_data()
    obj.sub_window.show()

    app.exec_()    

if __name__ == "__main__":
    _test_code()
