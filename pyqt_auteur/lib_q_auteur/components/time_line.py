#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from __future__ import division

import os, subprocess
from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
    import sys
    sys.path.insert(0, os.path.abspath("../../"))
    
from lib_q_auteur.components import ComponentWidget

class TimeLine(QtGui.QLabel):
    DEFAULT_STRETCH=20
    def __init__(self, parent=None):
        QtGui.QLabel.__init__(self, parent)
        self._duration = 0
        self.stretch()
        self.clips=[]
        
    @property
    def duration(self):
        return self._duration
        
    def set_duration(self, val):
        self._duration = val
        self.stretch()
            
    def stretch(self, val=DEFAULT_STRETCH):
        self.stretch_val = val/self.DEFAULT_STRETCH
        self.tl_width = self.stretch_val * self.duration
        self.duration_width = self.fontMetrics().width(str(self.duration))
        self.setFixedWidth(self.tl_width + self.duration_width)
    
    def set_model(self, model):
        self.model = model
        
    def update_data(self):
        self.set_duration(self.model.duration)
        self.clips = self.model.clips
        
    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
    
        option = QtGui.QTextOption()
        option.setAlignment(QtCore.Qt.AlignCenter)

        painter.fillRect(QtCore.QRect(0,0,self.width(),8), 
            QtGui.QColor("black"))
            
        painter.fillRect(QtCore.QRect(0,self.height()-8,self.width(),self.height()), 
            QtGui.QColor("black"))

        left_x = 0
        painter.save()
        for clip in self.clips:
            right_x = left_x + (clip.length * self.stretch_val)
            clip_rect = QtCore.QRectF(left_x,8,right_x-left_x,self.height()-16)
            painter.fillRect(clip_rect, QtGui.QColor("aqua")) 
            painter.drawRect(clip_rect) 

            painter.setPen(QtGui.QColor("black"))
            font = painter.font()
            font.setBold(True)
            font.setPointSize(24)
            painter.setFont(font)
            id = clip.id.lstrip("0")
            #painter.rotate(90)
            painter.drawText(clip_rect, id, option)

            painter.save()
            painter.translate(1,1)
            painter.setPen(QtGui.QColor("red"))
            painter.drawText(clip_rect, id, option)
            painter.restore()
                      
            left_x = right_x
        painter.restore()    
        
        x = 2
        while x < self.tl_width:
            
            painter.fillRect(QtCore.QRect(x,2,5,5), QtGui.QColor("white"))
            painter.fillRect(QtCore.QRect(x,self.height()-6,5,5), 
                QtGui.QColor("white"))
            
            x += 10

        text_point = QtCore.QPointF(self.tl_width, self.height()/2)
        painter.drawText(text_point, str(self.duration))   
        
        

class TimeLineWidget(ComponentWidget):
    def __init__(self, parent=None):
        ComponentWidget.__init__(self, parent)
        self.setWindowTitle(_("TimeLine"))
        self.icon = QtGui.QIcon(":images/clock.png")
        
        self.slider = QtGui.QSlider()
        self.slider.setRange(0,200)
        self.slider.setValue(TimeLine.DEFAULT_STRETCH)
        
        self.scroll_area = QtGui.QScrollArea()
        self.timeline = TimeLine(self)
        self.scroll_area.setWidget(self.timeline)
        self.scroll_area.setWidgetResizable(True)
    
        layout = QtGui.QHBoxLayout(self)
        layout.setMargin(0)
        layout.addWidget(self.slider)
        layout.addWidget(self.scroll_area)
        
        self.slider.valueChanged.connect(self.timeline.stretch)
    
    def set_model(self, model):
        self.timeline.set_model(model)
        model.attach_view(self)
        
    def update_data(self):
        self.timeline.update_data()
        self.timeline.stretch(self.slider.value())
        self.timeline.update()
        
def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")
    
    obj = TimeLineWidget() 
    
    from lib_q_auteur.components.data_model import DataModel
    
    model = DataModel() 
    test_proj = "/home/neil/Desktop/qauteur_prog.xml"
    model.load(test_proj)
    
    obj.set_model(model)
    obj.update_data()
    obj.sub_window.show()

    app.exec_()    

if __name__ == "__main__":
    _test_code()
