#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from __future__ import division

from PyQt4 import QtGui, QtCore

class ClipObject(object):
    def __init__(self, parent, runtime):
        self.parent_ = parent
        self.source_runtime = runtime
        self.start_time = 0
        self.end_time = 0    
        
        self._rect = None
        self._rectF = None
        self.is_hovered = False
        
        self.has_been_dragged = False

    @property
    def is_complete(self):
        '''
        a boolean to indicate whether this clip represents the entire source
        '''
        return (self.start_time == 0 and self.end_time == self.source_runtime)
        
    @property
    def start(self):
        return self.start_time/self.source_runtime *100

    @property
    def end(self):
        return self.end_time/self.source_runtime * 100

    def refresh_geometry(self):
        self._rect = None    
        self._rectF = None
        
    @property
    def rect(self):
        if self._rect is None:
            left_x = self.parent_.width() * self.start/100
            width = (self.end - self.start)/100 * self.parent_.width()
            self._rect = QtCore.QRect( 
                left_x,
                0,
                width, 
                self.parent_.height() ).adjusted(1,2,-1,-2)
        return self._rect
    
    @property
    def rectF(self):
        if self._rectF is None:
            self._rectF = QtCore.QRectF(self.rect) 
        return self._rectF
    
    def __lt__(self, other):
        return self.start_time < other.start_time
    
    def __repr__(self):
        return "clip start %s end %s"% (self.start_time, self.end_time)
    
class ClipsBucket(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setMouseTracking(True)
        self.drag_pixmap = QtGui.QPixmap(":images/clips.png")
        #self.setFixedHeight(40)
        self.text_option = QtGui.QTextOption()
        self.text_option.setAlignment(QtCore.Qt.AlignCenter)
        
        min_height = QtGui.QApplication.instance().fontMetrics().height()*1.2
        self.setMinimumHeight(min_height)
        
        self.setToolTip(
            _("drag me to the clips area to add this to your project"))
        
    @property
    def filename(self):
        return self._filename
    
    @property
    def run_time(self):
        return self._run_time
    
    def clear(self):
        self._filename = ""
        self._run_time = 0
        self.clips = []
        self.update()
    
    def set_source_info(self, filename, run_time):
        print ("new source for slider", filename, run_time)
        self._filename = filename
        self._run_time = run_time
        
        whole_clip = ClipObject(self, self.run_time)
        whole_clip.start_time = 0
        whole_clip.end_time = self.run_time
        self.clips = [whole_clip]
        
        self.update()

    def mark(self, timestamp):
        if timestamp > self.run_time:
            print ("timestamp > run_time... aborting mark")
            return        
        
        if len(self.clips) == 1: #first split 
            self.clips = []
            new_clip = ClipObject(self, self.run_time)
            new_clip.start_time = 0
            new_clip.end_time = timestamp
            self.clips.append(new_clip)
        
            new_clip = ClipObject(self, self.run_time)
            new_clip.start_time = timestamp
            new_clip.end_time = self.run_time
            self.clips.append(new_clip)
        else: #splitting an existing clip
            new_clip = ClipObject(self, self.run_time)
            for i in range(len(self.clips)):
                clip = self.clips[i]
                try:
                    end_time = self.clips[(i+1)].start_time
                except IndexError:
                    print(IndexError)
                    end_time = clip.end_time
                if clip.start_time < timestamp and timestamp < end_time:
                    clip.end_time = timestamp
                    clip.refresh_geometry()
                    new_clip.start_time = timestamp
                    new_clip.end_time = end_time
                    self.clips.append(new_clip)
                    break
            self.clips.sort()
        self.update()
    
    def mouseMoveEvent(self, event):
        pos_f = event.pos() 
        for clip in self.clips:
            clip.is_hovered = clip.rect.contains(pos_f)
        self.update()
    
    def mousePressEvent(self, event):
        self.start_drag()
        
    def leaveEvent(self, event):
        for clip in self.clips:
            clip.is_hovered = False
        self.update()
        
    def resizeEvent(self, event):
        for clip in self.clips:
            clip.refresh_geometry()
    
    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QColor("yellow"))#self.palette().highlightedText().color())
        colour = QtGui.QColor(self.palette().highlight().color())
        colour.setAlpha(120)
        painter.setBrush(colour)

        if len(self.clips) == 1:
            clip = self.clips[0]
            if clip.is_hovered:
                colour.setAlpha(255)
                painter.setBrush(colour)
            painter.drawRect(clip.rect)
            painter.drawText(clip.rectF, 
                _("UNSPLIT SOURCE - DRAG ME TO THE CLIPS AREA"), 
                self.text_option)
        else:
            i = 0
            for clip in self.clips:
                i += 1
                if clip.has_been_dragged:
                    continue
                if clip.is_hovered:
                    painter.save()
                    colour.setAlpha(255)
                    painter.setBrush(colour)
                    painter.drawRect(clip.rect)
                    painter.restore()
                else:
                    painter.drawRect(clip.rect)
                    painter.drawText(clip.rectF, "clip %s"% i, self.text_option)
                
                
    def start_drag(self):  
        if self.filename == "":
            print ("nothing to drag!")
        drag_clip = None
        for clip in self.clips:
            if clip.is_hovered:
                drag_clip = clip
        
        if drag_clip is None:
            return
        
        self.parent().emit(QtCore.SIGNAL("dragging clip"), True)
        d_text = "DRAGGED|%s"% self.filename
        if not drag_clip.is_complete:
            d_text += "|%s|%s"% (drag_clip.start_time, drag_clip.end_time)

        mimeData = QtCore.QMimeData()
        mimeData.setText(d_text)
        
        drag = QtGui.QDrag(self)
        drag.setMimeData(mimeData)

        drag.setPixmap(self.drag_pixmap)

        drag.setHotSpot(QtCore.QPoint(
            self.drag_pixmap.width()/2, self.drag_pixmap.height()/2))
        
        result = drag.start(QtCore.Qt.CopyAction)
        if result:
            print ("dropped")
            drag_clip.has_been_dragged = True
    
    
class ClipGrabbingSlider(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        
        self.slider = QtGui.QSlider(self)
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setRange(0,100)
        self.slider.setTickPosition(QtGui.QSlider.TicksBothSides)
        self.slider.setTickInterval(5)
                
        self.clips_bucket = ClipsBucket(self)
        self.clips_bucket.clear()
        
        layout = QtGui.QVBoxLayout(self)
        layout.setMargin(0)
        layout.setSpacing(0)
        layout.addWidget(self.slider)
        layout.addWidget(self.clips_bucket)
    
        #make some functions attributes of this wrapper widget
        self.set_source_info = self.clips_bucket.set_source_info
        self.mark = self.clips_bucket.mark
        self.setValue = self.slider.setValue
        self.sliderPressed = self.slider.sliderPressed
        self.sliderMoved = self.slider.sliderMoved
        self.sliderReleased = self.slider.sliderReleased
        self.setMinimumHeight(40)
        self.setMaximumHeight(self.clips_bucket.minimumHeight()*2.5)
    
    def sizeHint(self):
        return QtCore.QSize(120,60)
    
    def clear(self):
        self.setValue(0)
        self.clips_bucket.clear()
                
            
class _TestDropWidget(QtGui.QLabel):
    def __init__(self):
        QtGui.QLabel.__init__(self, "drop zone")
        self.setAcceptDrops(True)
        self.set_bg()

    def dragEnterEvent(self, event):
        if event.mimeData().hasText():
            self.set_bg(True)
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasText():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dragLeaveEvent(self, event):
        self.set_bg()

    def dropEvent(self, event):
        data = event.mimeData()
        print (data.text())
        self.set_bg()
        event.accept()

    def set_bg(self, active=False):
        if active:
            val = "background:yellow;"
        else:
            val = "background:green;"
        self.setStyleSheet(val)

def _test_main():
    app = QtGui.QApplication([])

    w = QtGui.QWidget()
    w.setFixedSize(200,300)
    
    slider = ClipGrabbingSlider(w)
    slider.set_source_info("test_video/test.ogv", 180)
    slider.mark(25)
            
    drop_widget = _TestDropWidget()
    
    layout = QtGui.QVBoxLayout(w)
    layout.addWidget(slider)
    layout.addWidget(drop_widget)
    w.show()
    
    app.exec_()    

if __name__ == "__main__":
    import gettext, os, sys
    
    gettext.install("auteur")
    
    sys.path.insert(0, os.path.abspath("../../"))
    from lib_q_auteur.components import q_resources
    
    _test_main()