#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

from PyQt4 import QtCore, QtGui

class SourcePropertiesDialog(QtGui.QDialog):
    def __init__(self, clip, parent=None):
        QtGui.QDialog.__init__(self, parent)
        
        self.setWindowTitle(_("Clip Properties"))
        
        label = QtGui.QLabel()
        image = clip.image.scaled(200,200)
        label.setPixmap(image)
        
        list_widget = QtGui.QListWidget()
        list_widget.setFont(QtGui.QFont("courier"))
        button_box = QtGui.QDialogButtonBox(self)
        button_box.setOrientation(QtCore.Qt.Horizontal)
        ok_but = button_box.addButton(button_box.Ok)
        
        ok_but.clicked.connect(self.accept)

        frame = QtGui.QFrame()
        layout = QtGui.QVBoxLayout(frame)
        layout.addWidget(label)
        layout.addWidget(button_box)
                
        splitter = QtGui.QSplitter()
        splitter.addWidget(list_widget)
        splitter.addWidget(frame)
        
        layout = QtGui.QVBoxLayout(self)
        layout.addWidget(splitter)
        
        keys = sorted(clip.properties.keys())
        
        for key in keys:
            item = QtGui.QListWidgetItem(
                "%s %s"% (key.lstrip("ID_").rjust(15),clip.properties[key]))
            list_widget.addItem(item)
            
        self.setMaximumSize(600,400)
            
    def sizeHint(self):
        return QtCore.QSize(600,300)

def _test_code():
    app = QtGui.QApplication([])
    
    import gettext
    gettext.install("auteur")

    import os, sys
    sys.path.insert(0, os.path.abspath("../../"))
    
    from lib_q_auteur.components.data_model import DataModel
    model = DataModel() 
    model.add_source_file("test_video/greetings_and_salutations.ogv", True)
    
    dl = SourcePropertiesDialog(model.clips[0]) 
    
    dl.exec_()    

if __name__ == "__main__":
    _test_code()
