#! /usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
##                                                                           ##
##  Copyright 2010, Neil Wallace <rowinggolfer@googlemail.com>               ##
##                                                                           ##
##  This program is free software: you can redistribute it and/or modify     ##
##  it under the terms of the GNU General Public License as published by     ##
##  the Free Software Foundation, either version 3 of the License, or        ##
##  (at your option) any later version.                                      ##
##                                                                           ##
##  This program is distributed in the hope that it will be useful,          ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of           ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            ##
##  GNU General Public License for more details.                             ##
##                                                                           ##
##  You should have received a copy of the GNU General Public License        ##
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.    ##
##                                                                           ##
###############################################################################

import os, re, subprocess, tempfile

from PyQt4 import QtCore, QtGui
from xml.dom import minidom

VALIDATE = True
try:
    from PyQt4.QtXmlPatterns import (   QXmlSchemaValidator, 
                                        QXmlSchema)
except ImportError:
    VALIDATE = False


class Still(object):
    '''
    A custom data object representing a still picture
    '''
    def __init__(self):
        self.filename = ""
        self.duration = 0
        self._id = ""
    
    @property
    def id(self):
        return self._id


class Source(object):
    '''
    a custom data type to describe a media source
    '''
    def __init__(self):
        self.filename = ""
        self.is_source = True
        self._image = None
        self._thumbnail = None
        self._icon = None
        self._screenshot_pos = "1.0"
        self._id = ""
        self._properties = None
        
    #def __repr__(self):
    #    return "SOURCE VIDEO %s %s"% (self.filename, self.length)
    
    @property
    def short_name(self):
        return os.path.basename(self.filename)
        
    @property
    def exists(self):
        '''
        is the file available?
        '''
        return os.path.exists(self.filename)
            
    @property    
    def id(self):
        '''
        A UNIQUE 4 digit code for the clip
        '''
        return self.filename
    
    @property
    def length(self):
        return float(self.properties.get("ID_LENGTH", 1))
    
    @property
    def screenshot_pos(self):
        if self._screenshot_pos is None:
            try:
                self._screenshot_pos = "%.1f"% float(self.start)
            except ValueError:
                self._screenshot_pos = "0.0"
        return self._screenshot_pos
    
    @property
    def icon(self):
        if self._icon is None:
            self._icon = QtGui.QIcon(self.thumbnail)
        return self._icon
    
    @property
    def thumbnail(self):
        if self._thumbnail is None:
            self._thumbnail = self.image.scaled(80,80)
        return self._thumbnail
    
    @property
    def image(self):
        if not self.exists:
            return QtGui.QPixmap(":images/unknown.png")
        if self._image is None:
            try:
                self._image = KNOWN_IMAGES[self.id]
                return self._image
            except KeyError:
                pass
            try:
                temp = tempfile.gettempdir()
                src = os.path.join(temp, "00000001.jpg")
                if os.path.exists(src):
                    os.remove(src)
                #use mplayer to grab a screenshot (ss)
                p = subprocess.Popen(["mplayer", "-really-quiet", "-ss", 
                    self.screenshot_pos, self.filename,
                    "-frames", "1", "-nosound", "-vo", 
                    "jpeg:outdir=%s"% temp])
                print (["mplayer", "-really-quiet", "-ss", 
                    self.screenshot_pos, self.filename,
                    "-frames", "1", "-nosound", "-vo", 
                    "jpeg:outdir=%s"% temp])
                p.wait()
                pixmap = QtGui.QPixmap(src)
            except Exception as e:
                print("unable to get image", e)
                pixmap = QtGui.QPixmap()
                
            if pixmap.isNull():
                pixmap = QtGui.QPixmap(":images/unknown.png")
            
            KNOWN_IMAGES[self.id] = pixmap
            self._image = pixmap
            
        return self._image
    
    @property
    def properties(self):
        if not self.exists:
            self._properties is None
            return {}
            
        if self._properties is None:
            try:
                self._properties = KNOWN_PROPERTIES[self.id]
                return self._properties
            except KeyError:
                pass
            self._properties = {}
            try:
                #use mplayer to get clip properties
                p = subprocess.Popen(["mplayer", "-vo", "null", "-ao", "null",
                "-frames", "0", "-identify", self.filename], 
                stdout=subprocess.PIPE)
                
                result = p.communicate()[0].decode("ascii")

                for val in (
                    "ID_FILENAME",
                    "ID_DEMUXER",
                    "ID_LENGTH", 
                    "ID_VIDEO_FORMAT", 
                    "ID_VIDEO_BITRATE",
                    "ID_VIDEO_WIDTH",
                    "ID_VIDEO_HEIGHT",
                    "ID_VIDEO_FPS",
                    "ID_VIDEO_ASPECT",
                    "ID_AUDIO_FORMAT",
                    "ID_AUDIO_BITRATE",
                    "ID_AUDIO_RATE",
                    "ID_AUDIO_NCH",
                    "ID_SEEKABLE",
                    "ID_CHAPTERS",                   
                ):
                
                    match=re.search("%s=(.*)\n"% val, result)
                    if match:
                        self._properties[val] = match.groups()[0]                

            except Exception as e:
                print("ERROR! unable to get clip properties!", e)
            
            KNOWN_PROPERTIES[self.id] = self._properties
    
        return self._properties
    
    @property
    def width(self):
        return int(self.properties.get("ID_VIDEO_WIDTH",0))

    @property
    def height(self):
        return int(self.properties.get("ID_VIDEO_HEIGHT",0))
    
    
class Clip(Source):
    '''
    a custom data type to describe a clip
    '''
    def __init__(self):
        Source.__init__(self)
        
        self.is_source = False 
        self._start = ""
        self._end = ""
        self._screenshot_pos = None
  
    def __repr__(self):
       return "CLIP %s"% self.id
 
    def __eq__(self, other):
        if type(self) != type(other):
            return False
        return self.id == other.id
    
    @property
    def id(self):
        return self._id
    
    @property
    def start(self):
        '''
        the start position of the clip within the source video file
        default is ""   (ie. the start)
        '''
        return self._start
    
    @property
    def end(self):
        '''
        the end position of the clip within the source video file
        default is ""   (ie. the end)
        '''
        return self._end
    
    @property
    def start_format(self):
        '''
        the start in format "00:00:00.0"
        '''
        if self.start == "":
            return
        start = float(self.start)
        assert start >= 0, (
            "FATAL ERROR: clip id (%s)has a negative start time"% self.id)
        hours = start//3600
        start = start - hours*3600
        mins = start//60
        secs = start-mins*60
        tenths = (secs - int(secs))*10
        return "%02d:%02d:%02d.%d"% (hours, mins, int(secs), tenths)
        
    @property
    def end_format(self):
        '''
        the end in format "00:00:00.0" (for --endpos)

        NOTE - in mplayer/mencoder --endpos is dependendent on the value of 
        --ss
        '''
        if self.end == "":
            return
        end = float(self.end)
        if self.start != "":
            end = end - float(self.start)
        assert end > 0, (
            "FATAL ERROR: clip id (%s) is of negative length"% self.id)
        hours = end//3600
        end = end - hours*3600
        mins = end//60
        secs = end-mins*60
        tenths = (secs - int(secs))*10
        return "%02d:%02d:%02d.%d"% (hours, mins, int(secs), tenths)
        
    @property
    def details(self):        
        return "Clip ID %s<br />source '%s'<br />Start %s<br />End %s"% (
                self.id,
                self.filename, 
                self.start,
                self.end)
                
    @property
    def is_partial(self):
        '''
        does the clip equate to the whole of the source?
        ''' 
        return not (self.start == "" and self.end == "")
    

    @property
    def length(self):
        if self.start != "":
            start = float(self.start)
        else:
            start = 0
        if self.end != "":
            return float(self.end) - start
        
        source_len = float(self.properties.get("ID_LENGTH", 0))

        return source_len - start
        
        
class DataModel(object):
    def __init__(self, doc=None):
        if doc is None:
            doc = self.new_doc()
        self.doc = doc
        
        self.history = [doc.toxml()]
        self.redo_list = []
        self.last_saved_pos = 0
        
        self.view_widgets = []
        self.signaller = QtCore.QObject()
        self._clip_id = 0
        self._duration = None
        
    def new_doc(self):
        doc = minidom.Document()
        top_level = doc.createElement("auteur")
        doc.appendChild(top_level)
    
        return doc

    def new_workspace(self):
        self.doc = self.new_doc()
        self._duration = 0
        self.get_max_clip_id()
        self.history = [self.xml]
        self.last_saved_pos = 0
        self.update_views()        
    
    def load(self, filename):
        try:
            doc = minidom.parse(filename)
        except Exception as e:
            raise IOError ("PARSING ERROR %s"% e)
        if not self.validate(doc.toxml()):
            raise IOError ("file is not a valid auteur XML file, sorry!")
        
        self.doc = doc
        self.get_max_clip_id()
        self.history = [self.xml]
        self.last_saved_pos = 0
        self.update_views()
        
    def load_string(self, xml):
        '''
        called when user edits the xml by hand
        '''
        try:
            doc = minidom.parseString(xml)
        except Exception as e:
            raise IOError ("PARSING ERROR %s"% e)
        if not self.validate(doc.toxml()):
            raise IOError ("file is not a valid auteur XML file, sorry!")
        self.doc = doc
        self.update_history()
        
    def validate(self, xml):
        if not VALIDATE:
            print ("WARNING - unable to check validity of XML")
            return True
        
        print ("ATTEMPTING VALIDATION WITH QtXmlSchema classes! (experimental)")

        f = QtCore.QFile(":schemas/auteur_v01.xsd")
        f.open(QtCore.QIODevice.ReadOnly)
        schema = QXmlSchema()
        schema.load(f)

        validator = QXmlSchemaValidator(schema)
        result = validator.validate(xml)
        
        return result
    
    def get_max_clip_id(self):
        self._clip_id = 0
        for node in self.doc.getElementsByTagName("clip"):
            id = node.getAttribute("clip-id")
            if id:
                i = int(id)
                if i > self._clip_id:
                    self._clip_id = i
    
    def attach_view(self, view_widget):
        assert view_widget.update_data
        self.view_widgets.append(view_widget)
    
    @property
    def undo_available(self):
        return len(self.history) > 1
    
    @property
    def redo_available(self):
        return len(self.redo_list) > 0
        
    @property
    def next_clip_id(self):
        '''
        a counter to ensure each clip is uniquely id'd
        '''
        self._clip_id += 1
        return "%04d"% self._clip_id
    
    @property
    def is_dirty(self):
        try:
            return self.history[self.last_saved_pos] != self.xml
        except IndexError:
            return True
        
    @property
    def has_changed(self):
        return self.history[-1] != self.xml
    
    @property
    def tempfile(self):
        '''
        save a temporary file and return it's path
        '''
        temp = tempfile.mktemp(suffix=".xml")
        self.save_file(temp)
        return temp
    
    def save_file(self, filename):
        try:
            f = open(filename, "w")
            f.write(self.pretty_xml)
            f.close()
            self.last_saved_pos = len(self.history)-1
            return (True, "")
        except Exception as e:
            return (False, e)
    
    def update_history(self):
        if self.has_changed:
            self.history.append(self.xml)
            self._duration = None
            self.update_views()
        if len (self.history) > 50:
            self.history = self.history[-50:]
            
    @property
    def has_sources(self):
        return not self.source_files == []
    
    def update_views(self):
        for view_widget in self.view_widgets:
            view_widget.update_data()
            
    def add_source_file(self, filename, use_as_clip=False):
        '''
        see if we already have a source node with this filename
        if we do.. all is well. otherwise - create it.
        schema is as follows
        <source location="~/Videos/examples.ogv">
            <clip/>
        </source>
        '''
        if filename in self.source_files:
            return        
            
        source_node = self.doc.createElement("source")
        source_node.setAttribute("location", filename.strip())
        
        self.doc.documentElement.appendChild(source_node)
        
        if use_as_clip:
            self.use_full_source_as_clip(filename)
        else:
            self.update_history()
    
    def use_full_source_as_clip(self, filename):
        '''
        add the source as a clip (no alteration to start pos or endpos)
        '''
        clip_node = self.doc.createElement("clip")
        clip_node.setAttribute("clip-id", self.next_clip_id)
        source_node = self.source_node_from_filename(filename.strip())
        source_node.appendChild(clip_node)
        
        self.update_history()
        
    def add_clip(self, filename, start, end):
        '''
        add a clip (filename must be known!)
        '''
        if filename not in self.source_files:
            print ("whoops.. clip filename not found!")
            print ("THIS SHOULDN'T HAPPEN!")
            return
        
        clip_node = self.doc.createElement("clip")
        clip_node.setAttribute("clip-id", self.next_clip_id)
        source_node = self.source_node_from_filename(filename.strip())
        source_node.appendChild(clip_node)
        clip_node.setAttribute("start", "%.1f"% start)
        clip_node.setAttribute("end", "%.1f"% end)
        
        self.update_history()
    
    def clip_from_selected(self, selected_clip):
        '''
        returns a tuple, (source, clip)
        '''
        source_node = self.source_node_from_filename(selected_clip.filename)
    
        clips = source_node.getElementsByTagName("clip")
        clip_node = None
        for clip in clips:
            if clip.getAttribute("clip-id") == selected_clip.id:
                clip_node = clip
        
        return source_node, clip_node
    
    def source_from_filename(self, filename):
        for source in self.sources:
            if source.filename == filename.strip():
                return source
    
    def split_clip(self, selected_clip, time_stamp):
        print("splitting clip", selected_clip, time_stamp)
        
        source_node, current_node = self.clip_from_selected(selected_clip)
        
        current_node.setAttribute("end", "%.1f"% time_stamp)
        
        new_clip_id = self.next_clip_id
        new_clip_node = self.doc.createElement("clip")
        new_clip_node.setAttribute("clip-id", new_clip_id)
        new_clip_node.setAttribute("start", "%.1f"% time_stamp)
        
        source_node.appendChild(new_clip_node)
        
        self.update_history()
        return new_clip_id
    
    def delete_clip(self, selected_clip):
        '''
        deletes the selected clip
        '''
        print("deleting clip", selected_clip) 
        source_node, clip_node = self.clip_from_selected(selected_clip)
        print(source_node.removeChild(clip_node))
        self.update_history()
    
    def source_node_from_filename(self, filename):
        '''
        returns the source_file node for a known filename
        '''
        l = []
        for source_node in self.doc.getElementsByTagName("source"):
            if source_node.getAttribute("location") == filename:
                return source_node
    
    def source_location_from_clipnode(self, clipnode):
        '''
        returns the file location for a known clipnode
        '''
        source_node = clipnode.parentNode
        return source_node.getAttribute("location")
    
    @property
    def number_of_sources(self):
        '''
        returns the current number of known sources
        '''
        return len(self.source_files)
    
    @property
    def source_files(self):
        '''
        returns a list of source_file locations
        eg. ["/home/user/Videos/holiday1.avi", ...]
        '''
        l = []
        for source_node in self.doc.getElementsByTagName("source"):
            location_text = source_node.getAttribute("location")
            l.append(location_text)
        return l
    
    @property
    def number_of_clips(self):
        '''
        returns the current number of known sources
        '''
        return len(self.clips)
        
    @property
    def clips(self):
        '''
        returns a list of clips (in order!)
        '''
        clips = {}
        order = []
        for clip_node in self.doc.getElementsByTagName("clip"):
            clip = Clip()
            clip.filename = self.source_location_from_clipnode(clip_node)
            clip._id = clip_node.getAttribute("clip-id")
            clip._start = clip_node.getAttribute("start")
            clip._end = clip_node.getAttribute("end")
            clips[clip._id] = clip
            order.append(clip._id)
            
        ## look for a custom clip order node.. if it doesn't exist, then
        ## use Alphabetical order ie. 0001 0002 0003
            
        clip_id_order = self.clip_id_order
        if clip_id_order is None:
            order.sort()
            clip_id_order = order
            
        ordered_clips = []
        for clip_id in clip_id_order:
            clip = clips.get(clip_id, None)
            if id is None:
                print ("WARNING - garbage found in clip-order node")
            else:
                ordered_clips.append(clip)
        
        return ordered_clips
    
    @property
    def duration(self):
        if self._duration is None:
            self._duration = 0
            for clip in self.clips:
                self._duration += clip.length
        return self._duration
        
    def order_node(self, create = False):
        '''
        returns the clip order node (creating one if necessary)
        '''
        order_nodes = self.doc.getElementsByTagName("clip-order")
        if order_nodes == []:
            if create:
                order_node = self.doc.createElement("clip-order")
                self.doc.documentElement.appendChild(order_node)
            else:
                order_node = None
        else:
            order_node = order_nodes[0]
        return order_node
        
    @property
    def clip_id_order(self):
        '''
        returns a list (text node) describing clip order
        eg ["0001", "0003", "0002"] or None if this node doesn't exist
        '''
        order_node = self.order_node()
        if order_node is None:
            return None
        
        order_string = order_node.firstChild.data
        order = order_string.replace("\n"," ").split(" ")
        
        while "" in order:
            order.remove("")
        
        return order
    
    def advance_clip(self, selected_clip):
        '''
        move a clip forward in the timeline
        '''
        print ("advance clip", selected_clip)
        clips = self.clips[:]  #quick copy
        if selected_clip not in clips:
            print ("not found")
            return
        
        i = clips.index(selected_clip)
        clips.pop(i)
        order_string = ""
        for clip in clips[:i-1] + [selected_clip] + clips[i-1:]:
            order_string += "%s "% clip.id
        
        order_node = self.order_node(True)
        for child in order_node.childNodes:
            order_node.removeChild(child)
        
        text_node = self.doc.createTextNode(order_string.strip(" "))
        order_node.appendChild(text_node)
        
        self.update_history()
        
    def retreat_clip(self, selected_clip):
        '''
        move a clip backwards - TODO - LOTS OF REPEATED CODE HERE
        '''
        print ("retreat clip", selected_clip)
        clips = self.clips[:]  #quick copy
        if selected_clip not in clips:
            print ("not found")
            return
        
        i = clips.index(selected_clip)
        clips.pop(i)
        order_string = ""
        for clip in clips[:i+1] + [selected_clip] + clips[i+1:]:
            order_string += "%s "% clip.id
        
        order_node = self.order_node(True)
        for child in order_node.childNodes:
            order_node.removeChild(child)
        
        text_node = self.doc.createTextNode(order_string.strip(" "))
        order_node.appendChild(text_node)
        
        self.update_history()
        
    @property
    def sources(self):
        '''
        returns a list of sources
        '''
        sources = []
        for source_file in self.source_files:
            source = Source()
            source.filename = source_file
            
            sources.append(source)
            
        return sources

        
    @property
    def xml(self):
        '''
        returns the current xml
        '''
        return self.doc.toxml()
    
    @property
    def pretty_xml(self):
        '''
        the underlying pretty xml
        NOTE - minidom's pretty xml function introduces WAY too much 
        whitespace if any text nodes are present.
        hence jump through a hoop a little here....
        also.. I will eventualy want attributes in this order
        clip-id, start, end..
        but that's on the TODO list
        '''
        
        pretty = self.xml.replace("><",">\n<")
        pretty = pretty.replace("\n<source","\n\t<source")
        pretty = pretty.replace("\n</source","\n\t</source")
        pretty = pretty.replace("\n<clip","\n\t\t<clip")
        
        return pretty
    
    def _restore_last_history(self):
        doc = minidom.parseString(self.history[-1])
        self.doc = doc
        self.update_history()
        self.update_views()
        
    def undo(self):
        if self.undo_available:
            self.redo_list.append(self.history.pop(-1))
            self._restore_last_history()
            
    def redo(self):
        if self.redo_available:
            self.history.append(self.redo_list.pop(-1))
            self._restore_last_history()
            
    @property
    def xml_widget_text(self):
        '''
        the text supplied to the xml_widget
        '''
        return self.pretty_xml
    
    @property
    def biggest_source_dimension(self):
        '''
        finds the tallest source video, returns it's dimensions
        '''
        width, height = 0, 0
        for source in self.sources:
            if height < source.height:
                height = source.height
                width = source.width
        
        return "%s:%s"% (width, height)
        
    @property
    def stills(self):
        ##TODO
        return "I need to allow still images into the model"
        
    def mencoder_command_list(self, outfile = None):
        '''
        the current mencoder_commands for the data
        
        based on this suggestion from auteur founder and advocate, klaatu 

        mencoder evildead.ogv -ss 00:13:13 -endpos 00:00:13 -oac pcm -ovc lavc 
        armyofdarkness.ogv -ss 00:66:06 -endpos 00:06:06 -oac pcm -ocv lavc -o 
        ./render/evilcomposite.avi
                
        UPDATED VERSION 2011-01-11
        mencoder foo.flv -ss 00:00:03 -endpos 43 -oac pcm -ofps 24 -ovc lavc 
        -vf scale=320:240 bar.ogv -ss 00:01:23 -endpos 12 -oac pcm -ofps 24 
        -ovc lavc -vf scale=320:240 -o baz.avi
        '''        
        
        if outfile is None:
            outfile = "[YOUR_OUTFILE]"
        
        commands = [    "mencoder", 
                        "-o", outfile, 
                        "-vf", "scale=%s"% self.biggest_source_dimension 
                    ]
                    
        insert_point = len(commands)
        for clip in self.clips:
            commands += [clip.filename]
            if clip.start != "":
                commands += ["-ss", clip.start_format]
            if clip.end != "":
                commands += ["-endpos", clip.end_format]
                
            commands += [   "-oac", "pcm" ,
                            "-ofps", "24", 
                            "-ovc", "lavc", 
                            ]
    
        return commands, insert_point
    
    @property
    def mencoder_string(self):
        command = ""
        commands, insert_point = self.mencoder_command_list()
        commands = commands[:insert_point] + ["[OPTIONS]"] + commands[insert_point:]
        for arg in commands:
            command += arg.replace(" ", "\ ") + " "
        
        return command

KNOWN_IMAGES, KNOWN_PROPERTIES = {}, {}

def _test_model():
    import gettext
    gettext.install("auteur")
    
    import sys
    sys.path.insert(0, os.path.abspath("../../"))
    from lib_q_auteur.components import q_resources
    
    obj = DataModel() 
    
    #note the space at the end of the file name.. check prog handles this ok
    obj.add_source_file("test_video/greetings_and_salutations.ogv ")    
    print(obj.source_files)
    
    obj.add_clip("test_video/greetings_and_salutations.ogv", 0.5, 1)
    obj.add_clip("test_video/greetings_and_salutations.ogv", 1.5, 2 )
    
    ##todo - add a clip
    print(obj.pretty_xml)
    print("file duration %s seconds"% obj.sources[0].length)
    print (obj.mencoder_string)
    app=QtCore.QCoreApplication([])
    obj.validate(obj.xml)
    
    clip = obj.clips[1]
    print (obj.xml)
    obj.advance_clip(clip)
    print (obj.xml)
    
if __name__ == "__main__":
    _test_model()
